import java.util.UUID

import model.Position
import model.entity.{EmptyEntity, Enemy}
import org.scalatest.{FlatSpec, Matchers}

class EntitySpec extends FlatSpec with Matchers {
  Enemy.getClass.getSimpleName should "create correct" in {
    val enemy = Enemy(UUID.randomUUID(), "Boeser Gegner", Position(15, 15), health = 100, atk = 10, defense = 10)
    enemy.health shouldBe 100
  }

  EmptyEntity.getClass.getSimpleName should "create correct" in {
    val empty = EmptyEntity(UUID.randomUUID(), "Empty", Position(15, 15))
    empty.name shouldBe "Empty"
  }

}
