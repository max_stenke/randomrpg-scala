import java.util.UUID

import model.actions.{Action, ActionType}
import model.entity._
import model.game.{ModelActionExecuted, ModelDied, ModelStartedGame}
import model.{Game, Position}
import org.scalatest.{FlatSpec, Matchers}


class GameSpec extends FlatSpec with Matchers {

  Game.getClass.getSimpleName should "should generate 30 x 30 world on start up" in {
    val game = Game()
    game.rowCount() should ===(30)
    game.columnCount() should ===(30)
  }

  it should "return correct world" in {
    val game = Game()
    game.getWorld.rows shouldBe game.rowCount()
    game.getWorld.columns shouldBe game.columnCount()
  }

  it should "init player in center auf game start" in {
    val game = Game()
    game.startGame()
    val player = game.getPlayer
    player.exists(p => p.position.equals(Position(15, 15))) should be(true)
  }

  it should "return correct row and column count" in {
    val game = Game()
    game.startGame()
    game.rowCount() should ===(30)
    game.columnCount() should ===(30)
  }

  it should "place entity in correct place" in {
    val game = Game()
    game.startGame()
    val entity = EmptyEntity(UUID.randomUUID(), "empty", Position(5, 5))
    game.placeEntityObject(entity)

    game.getCellContent(5, 5) should be equals entity
  }

  it should "return empty entity when empty cell is called" in {
    val game = Game()
    val entity = EmptyEntity(UUID.randomUUID(), "empty", Position(5, 5))

    game.getCellContent(5, 5) should be equals entity
  }

  it should "return correct actions for a field" in {
    val game = Game()
    game.startGame()
    val mine = Mine(UUID.randomUUID(), "Spruce", Position(10, 10), amountOfStone = 50, player = None)
    val actions = List(Action("C", "Collect Resources", ActionType.Collect), Action("W", "Wait", ActionType.WAIT))
    game.placeEntityObject(mine)
    game.getActionsForField(Position(10, 10)) should be equals actions
  }

  it should "return additional actions if field has enemy" in {
    val game = Game()
    game.startGame()
    val forest = Forest(UUID.randomUUID(), "Spruce", Position(10, 10), 50, enemy = None, player = None)
    val actions = List(Action("C", "Collect Resources", ActionType.Collect), Action("W", "Wait", ActionType.WAIT), Action("A", "Attack Enemy", ActionType.ATTACK), Action("F", "Flee", ActionType.FLEE))
    game.placeEntityObject(forest)
    game.getActionsForField(Position(10, 10)) should be equals actions
  }

  it should "execute collect wood resources" in {
    val game = Game()
    game.startGame()
    val player = game.getPlayer
    val forest = Forest(UUID.randomUUID(), "Spruce", Position(15, 15), 50, enemy = None, player = player)
    game.placeEntityObject(forest)
    val action = Some(Action("C", "Collect Resources", ActionType.Collect))
    game.executeAction(action)

    game.getPlayer.get.inventory.wood should be > player.get.inventory.wood

  }

  it should "collect stone resources" in {
    val game = Game()
    game.startGame()
    val player = game.getPlayer
    val mine = Mine(UUID.randomUUID(), "Sandstone", Position(15, 15), amountOfStone = 25, player = player)
    game.placeEntityObject(mine)
    val action = Some(Action("C", "Collect Resources", ActionType.Collect))
    game.executeAction(action)

    game.getPlayer.get.inventory.stone should be > player.get.inventory.stone
  }

  it should "collect nothing on other fields" in {
    val game = Game()
    game.startGame()
    val player = game.getPlayer
    val mine = Lake(UUID.randomUUID(), "Sandstone", Position(15, 15), player = player)
    game.placeEntityObject(mine)
    val action = Some(Action("C", "Collect Resources", ActionType.Collect))
    game.executeAction(action) shouldBe "noting to get here"
  }

  it should "do nothing on collect when filed is not defined" in {
    val game = Game()
    game.startGame()
    val player = game.getPlayer
    val mine = Lake(UUID.randomUUID(), "Sandstone", Position(15, 15), player = player)
    game.placeEntityObject(mine)
    val action = Some(Action("C", "Collect Resources", ActionType.Collect))
    game.executeAction(action) shouldBe "noting to get here"
  }

  it should "execute attack" in {
    val game = Game()
    game.startGame()
    val player = game.getPlayer
    val enemy = Some(Enemy(UUID.randomUUID(), "Weakling", Position(15, 15), 'R', 20, atk = 5, defense = 5))
    val forest = Forest(UUID.randomUUID(), "Spruce", Position(15, 15), 50, enemy = enemy, player = player)
    game.placeEntityObject(forest)
    val action = Some(Action("A", "Attack", ActionType.ATTACK))
    game.executeAction(action)

    game.getCellContent(15, 15).enemy.get.health should be < enemy.get.health
    game.getCellContent(15, 15).player.get.health should be < player.get.health
  }

  it should "execute attack an kill enemy" in {
    val game = Game()
    game.startGame()
    val player = game.getPlayer
    val enemy = Some(Enemy(UUID.randomUUID(), "Weakling", Position(15, 15), 'R', 1, atk = 5, defense = 5))
    val forest = Forest(UUID.randomUUID(), "Spruce", Position(15, 15), 50, enemy = enemy, player = player)
    game.placeEntityObject(forest)
    val action = Some(Action("A", "Attack", ActionType.ATTACK))
    game.executeAction(action)

    game.getCellContent(15, 15).enemy should be(scala.None)
  }

  it should "execute attack an kill you" in {
    val game = Game()
    game.startGame()
    val player = game.getPlayer
    val enemy = Some(Enemy(UUID.randomUUID(), "Chuck Norris", Position(15, 15), 'R', 1000, atk = 10000, defense = 5100))
    val forest = Forest(UUID.randomUUID(), "Spruce", Position(15, 15), 50, enemy = enemy, player = player)
    game.placeEntityObject(forest)
    val action = Some(Action("A", "Attack", ActionType.ATTACK))
    game.executeAction(action)

    game.getPlayer.get.health shouldBe 0
  }

  it should "execute no action for other actions than collect and attack" in {
    val game = Game()
    game.startGame()
    val action = Some(Action("N", "Nothing Defined here", ActionType.WAIT))
    game.executeAction(action) shouldBe "Nothing to do here"
  }

  it should "execute no action for none" in {
    val game = Game()
    game.startGame()
    game.executeAction(None) shouldBe "Something went wrong..."
  }

  it should "move north" in {
    val game = Game()
    game.startGame()

    game.getCurrentPosition.rowIdx shouldBe 15

    game.move("north")

    game.getCurrentPosition.rowIdx shouldBe 14
  }

  it should "move south" in {
    val game = Game()
    game.startGame()

    game.getCurrentPosition.rowIdx shouldBe 15

    game.move("south")

    game.getCurrentPosition.rowIdx shouldBe 16
  }

  it should "move east" in {
    val game = Game()
    game.startGame()

    game.getCurrentPosition.columnIdx shouldBe 15

    game.move("east")

    game.getCurrentPosition.columnIdx shouldBe 16
  }

  it should "move west" in {
    val game = Game()
    game.startGame()

    game.getCurrentPosition.columnIdx shouldBe 15

    game.move("west")

    game.getCurrentPosition.columnIdx shouldBe 14
  }

  it should "show enemy when next field hast on" in {
    val game = Game()
    game.startGame()
    val enemy = Some(Enemy(UUID.randomUUID(), "Chuck Norris", Position(14, 15), 'R', 1000, atk = 10000, defense = 5100))
    val forest = Forest(UUID.randomUUID(), "Spruce", Position(14, 15), 50, enemy = enemy, player = None)
    game.placeEntityObject(forest)
    game.move("north")

    game.getBattleLog.head.event shouldBe "Encounter: Chuck Norris, health: 1000"
  }

  //Events

  ModelActionExecuted.getClass.getSimpleName should "create" in {
    val event = ModelActionExecuted
    event.apply()
  }
  ModelDied.getClass.getSimpleName should "create" in {
    val event = ModelDied
    event.apply()
  }
  ModelStartedGame.getClass.getSimpleName should "create" in {
    val event = ModelStartedGame
    event.apply()
  }
}
