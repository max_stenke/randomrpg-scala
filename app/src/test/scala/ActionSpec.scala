import controller.{ActionExecuted, Died, StartedGame}
import model.actions.{Action, ActionType}
import org.scalatest.{FlatSpec, Matchers}

class ActionSpec extends FlatSpec with Matchers {
  Action.getClass.getSimpleName should "create correct" in {
    val action = Action("A", "Action", ActionType.MOVE)
    action.identifier shouldBe "A"
    action.description shouldBe "Action"
    action.actionType shouldBe ActionType.MOVE
  }




  //Events

  ActionExecuted.getClass.getSimpleName should "create" in {
    val event = ActionExecuted
    event.apply()
  }
  Died.getClass.getSimpleName should "create" in {
    val event = Died
    event.apply()
  }
  StartedGame.getClass.getSimpleName should "create" in {
    val event = StartedGame
    event.apply()
  }

}
