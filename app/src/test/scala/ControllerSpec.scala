import java.util.UUID

import controller.Controller
import model.actions.{Action, ActionType}
import model.battleLog.BattleLogStack
import model.{Game, Position}
import model.entity.{EmptyEntity, Enemy, Forest, Mine}
import org.scalatest.{FlatSpec, Matchers}
import utils.Asciis

class ControllerSpec extends FlatSpec with Matchers {

  Controller.getClass.getSimpleName should "create world on start" in {
    val controller = Controller(Game())
    controller.startGame()
    controller.getWorld.rows shouldBe controller.rowCount
    controller.getWorld.columns shouldBe controller.columnCount
  }

  it should "get correct cell" in {
    val game = Game()
    val controller = Controller(game)
    controller.startGame()

    val entity = EmptyEntity(UUID.randomUUID(), "empty", Position(5, 5))
    game.placeEntityObject(entity)

    controller.getCellContent(5, 5) shouldBe entity
  }

  it should "get correct actions" in {
    val game = Game()
    val controller = Controller(game)
    controller.startGame()

    val mine = Mine(UUID.randomUUID(), "Spruce", Position(15, 15), amountOfStone = 50, player = None)
    val actions = List(Action("C", "Collect Resources", ActionType.Collect), Action("W", "Wait", ActionType.WAIT))
    game.placeEntityObject(mine)

    controller.getActionsForField(Position(15, 15)) shouldBe actions
  }

  it should "execute action" in {
    val game = Game()
    val controller = Controller(game)
    controller.startGame()

    val player = controller.getPlayer
    val forest = Forest(UUID.randomUUID(), "Spruce", Position(15, 15), 50, enemy = None, player = player)
    game.placeEntityObject(forest)
    val action = Some(Action("C", "Collect Resources", ActionType.Collect))
    game.executeAction(action)

    game.getPlayer.get.inventory.wood should be > player.get.inventory.wood
  }

  it should "move north" in {
    val game = Game()
    val controller = Controller(game)
    controller.startGame()

    controller.getCurrentPosition.rowIdx shouldBe 15

    controller.move("north")

    controller.getCurrentPosition.rowIdx shouldBe 14
  }

  it should "get correct position" in {
    val game = Game()
    val controller = Controller(game)
    controller.startGame()

    controller.getCurrentPosition shouldBe Position(15, 15)
  }

  it should "execute attack" in {
    val game = Game()
    val controller = Controller(game)
    controller.startGame()
    val player = controller.getPlayer
    val enemy = Some(Enemy(UUID.randomUUID(), "Weakling", Position(15, 15), 'R', 20, atk = 5, defense = 5))
    val forest = Forest(UUID.randomUUID(), "Spruce", Position(15, 15), 50, enemy = enemy, player = player)
    game.placeEntityObject(forest)
    val action = Some(Action("A", "Attack", ActionType.ATTACK))
    controller.executeAction(action)

    controller.getCellContent(15, 15).enemy.get.health should be < enemy.get.health
    controller.getCellContent(15, 15).player.get.health should be < player.get.health
  }

  it should "return enemy print string correct" in {
    val game = Game()
    val controller = Controller(game)
    controller.startGame()
    val player = controller.getPlayer
    val enemy = Some(Enemy(UUID.randomUUID(), "Weakling", Position(15, 15), 'R', 20, atk = 5, defense = 5))
    val forest = Forest(UUID.randomUUID(), "Spruce", Position(15, 15), 50, enemy = enemy, player = player)
    game.placeEntityObject(forest)

    controller.printEnemies(Position(15, 15)) shouldBe "A Weakling appears"
  }

  it should "return correct battle log" in {
    val game = Game()
    val controller = Controller(game)
    controller.startGame()

    controller.getBattleLog.head.event shouldBe "Game Started"
  }

  it should "return correct cell content as List[String]" in {
    val game = Game()
    val controller = Controller(game)
    controller.startGame()

    val forest = Forest(UUID.randomUUID(), "Spruce", Position(10, 10), 50, enemy = None, player = None)
    game.placeEntityObject(forest)
    controller.getCellContentText(10, 10).head shouldBe Asciis.FOREST.toString

    val player = controller.getPlayer
    val forest2 = Forest(UUID.randomUUID(), "Spruce", Position(15, 15), 50, enemy = None, player = player)
    game.placeEntityObject(forest2)
    controller.getCellContentText(15, 15).head shouldBe Asciis.FOREST.toString

  }

  it should "return correct available actions as List[String]" in {
    val game = Game()
    val controller = Controller(game)
    controller.startGame()

    val player = controller.getPlayer
    val forest = Forest(UUID.randomUUID(), "Spruce", Position(10, 10), 50, enemy = None, player = player)
    game.placeEntityObject(forest)
    controller.getAvailableActionsText.head shouldBe "Collect Resources"
  }

  it should "return correct player text as List[String]" in {
    val game = Game()
    val controller = Controller(game)
    controller.startGame()

    val player = controller.getPlayer
    val text = List("Health: " + player.get.health.toString,
      "Attack: " + player.get.atk.toString,
      "Defense: " + player.get.defense.toString,
      "Wood: " + player.get.inventory.wood.toString,
      "Stone: " + player.get.inventory.stone.toString)
    val forest = Forest(UUID.randomUUID(), "Spruce", Position(10, 10), 50, enemy = None, player = player)
    game.placeEntityObject(forest)
    controller.getPlayerText shouldBe text
  }

  it should "return correct battle log as List[String]" in {
    val game = Game()
    val controller = Controller(game)
    controller.startGame()

    controller.getBattleLogText.head shouldBe "Game Started"
  }

}
