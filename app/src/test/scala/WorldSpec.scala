
import java.util.UUID

import model.Position
import model.entity.{EmptyEntity, Forest, Mine, Player}
import model.world.World
import org.scalatest.{FlatSpec, Matchers}

class WorldSpec extends FlatSpec with Matchers {
  World.getClass.getSimpleName should "throw IllegalArgumentException when column or row count is negative" in {
    a[IllegalArgumentException] should be thrownBy {
      World(-1, 1)
    }
    a[IllegalArgumentException] should be thrownBy {
      World(1, -1)
    }
    a[IllegalArgumentException] should be thrownBy {
      World(-1, -1)
    }
  }

  it should "return true when position in bound" in {
    World(25, 25).inBound(Position(5, 5)) should be(true)
  }

  it should "return false when position out of bound" in {
    World(25, 25).inBound(Position(-5, -5)) should be(false)
  }

  it should "place entity in correct place" in {
    val entity = EmptyEntity(UUID.randomUUID(), "empty", Position(5, 5))
    val world = World(25, 25)
    world.setEntityAt(entity)
    entity should be equals world.entityObjectAt(5, 5).get
  }

  it should "return same entity with both objectAt methods" in {
    val world = World(25, 25)
    world.setEntityAt(EmptyEntity(UUID.randomUUID(), "empty", Position(5, 5)))
    world.entityObjectAt(Position(5, 5)).get should be equals world.entityObjectAt(5, 5).get
  }

  it should "remove correct entity" in {
    val entity = EmptyEntity(UUID.randomUUID(), "empty", Position(5, 5))
    val world = World(25, 25)
    world.setEntityAt(entity)

    world.entityObjectAt(5, 5).isDefined should be(true)

    world.removeEntity(entity)

    world.entityObjectAt(5, 5).isDefined should be(false)
  }

  it should "find player position" in {
    val world = World(1, 1)
    world.setEntityAt(Forest(UUID.randomUUID(), "wald", Position(0, 0), player = Some(Player(UUID.randomUUID(), "TestPlayer", Position(0, 0), 100, atk = 50, defense = 20)), enemy = None, amountOfWood = 0))
    world.findPlayerPosition() should be equals Position(1, 1)
  }

  it should "find player" in {
    val world = World(1, 1)
    val player = Player(UUID.randomUUID(), "TestPlayer", Position(0, 0), 100, atk = 50, defense = 20)
    world.setEntityAt(Mine(UUID.randomUUID(), "wald", Position(0, 0), player = Some(player), amountOfStone = 0))
    world.findPlayer().get should be equals player
  }
}
