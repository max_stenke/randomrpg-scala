package controller

import model.actions.Action
import model.battleLog.BattleLogEntry
import model.{Game, Position}
import model.entity.{Entity, Player}
import model.game.{ModelActionExecuted, ModelDied, ModelStartedGame}
import model.world.World

case class Controller(model: Game) extends TController {
  listenTo(model)

  reactions += {
    case _: ModelStartedGame => publish(StartedGame())
    case _: ModelActionExecuted => publish(ActionExecuted())
    case _: ModelDied => publish(Died())
  }

  override def startGame(): Unit = {
    model.startGame()
  }

  override def rowCount: Int = {
    model.rowCount()
  }

  override def columnCount: Int = {
    model.columnCount()
  }

  override def getCellContent(x: Int, y: Int): Entity = {
    model.getCellContent(x, y)
  }

  override def getActionsForField(position: Position): List[Action] = {
    model.getActionsForField(position)
  }

  override def executeAction(action: Option[Action]): String = {
    model.executeAction(action)
  }

  override def move(direction: String): Unit = {
    model.move(direction)
  }

  override def getCurrentPosition: Position = {
    model.getCurrentPosition
  }

  override def printEnemies(position: Position): String = {
    model.printEnemy(position)
  }

  override def getWorld: World = {
    model.getWorld
  }

  override def getBattleLog: List[BattleLogEntry] = {
    model.getBattleLog
  }

  override def getPlayer: Option[Player] = {
    model.getPlayer
  }

  override def getCellContentText(x: Int, y: Int): List[String] = {
    model.getCellContentToText(x, y)
  }

  override def getAvailableActionsText: List[String] = {
    model.getActionsToText()
  }

  override def getPlayerText: List[String] = {
    model.getPlayerInformation()
  }

  override def getBattleLogText: List[String] = {
    model.getBattleLogText()
  }
}
