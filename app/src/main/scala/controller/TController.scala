package controller

import model.Position
import model.actions.Action
import model.battleLog.BattleLogEntry
import model.entity.{Entity, Player}
import model.world.World

import scala.swing.Publisher
import scala.swing.event.Event

case class StartedGame() extends Event

case class ActionExecuted() extends Event

case class Died() extends Event

trait TController extends Publisher {
  /**
    * Starts a new generated game with random entities
    */
  def startGame(): Unit

  /**
    * @return the number of rows of the world
    */
  def rowCount: Int

  /**
    * @return the number of columns of the world
    */
  def columnCount: Int

  /**
    *
    * @param x Row Value of the World
    * @param y Column Value of the world
    * @return the character of the entity wich is placed
    *         on a given Position
    */
  def getCellContent(x: Int, y: Int): Entity

  def getActionsForField(position: Position): List[Action]

  def executeAction(action: Option[Action]): String

  def move(direction: String): Unit

  def getCurrentPosition: Position

  /**
    * Prints the enemy name of the current position
    *
    * @return
    */
  def printEnemies(position: Position): String

  /**
    *
    * @return current world
    */
  def getWorld: World

  def getBattleLog: List[BattleLogEntry]

  def getPlayer: Option[Player]

  def getCellContentText(x:Int, y:Int): List[String]

  def getAvailableActionsText: List[String]

  def getPlayerText: List[String]

  def getBattleLogText: List[String]

}
