package view.gui

import java.awt.Color

import controller.Controller
import model.entity.Entity
import utils.Asciis

import scala.swing.{Dimension, GridPanel, Label}

class SWorldGrid(controller: Controller) extends GridPanel(controller.getWorld.rows, controller.getWorld.columns) {

  preferredSize = new Dimension(100, 100)
  background = Color.BLACK
  controller.getWorld.world.flatten.foreach(entity => {
    if (entity.player.isDefined) {
      contents += new GridPanel(1, 2) {
        background = Color.BLACK
        contents += getLabel(entity)
        contents += new Label(Asciis.PLAYER.toString) {
          tooltip = "This is you"
          foreground = Color.RED
        }
      }
    } else {
      contents += getLabel(entity)
    }
  })

  private def getLabel(entity: Entity): Label = {
    new Label(entity.ascii_symbol.toString) {
      tooltip = entity.name
      entity.ascii_color match {
        case "green" => foreground = Color.GREEN
        case "cyan" => foreground = Color.CYAN
        case "blue" => foreground = new Color(0, 0, 255)
        case "magenta" => foreground = Color.MAGENTA
        case _ => foreground = Color.WHITE
      }
    }
  }

}
