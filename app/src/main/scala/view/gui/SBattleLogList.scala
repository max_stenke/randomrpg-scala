package view.gui


import java.awt.Color

import controller.Controller

import scala.swing.ListView.Renderer
import scala.swing.{ListView, ScrollPane}

class SBattleLogList(controller: Controller) extends ScrollPane {
  contents = new ListView(controller.getBattleLog) {
    background = Color.BLACK
    foreground = Color.WHITE
    fixedCellWidth = 200
    renderer = Renderer(_.event)
  }
}
