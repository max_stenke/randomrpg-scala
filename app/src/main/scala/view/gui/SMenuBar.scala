package view.gui

import controller.Controller

import scala.swing.event.Key
import scala.swing.{Action, Menu, MenuBar, MenuItem}

class SMenuBar(controller: Controller) extends MenuBar {

  contents += new Menu("RandomRPG") {
    mnemonic = Key.G

    contents += new MenuItem(Action("Start Game") {
      controller.startGame()
    })
  }
}
