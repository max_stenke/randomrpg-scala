package view.gui

import java.awt.Color

import controller.{ActionExecuted, Controller, Died, StartedGame}

import scala.swing.event.{Key, KeyPressed}
import scala.swing.{BorderPanel, FlowPanel, Label, Reactor}

class SPanel(controller: Controller) extends BorderPanel with Reactor {
  focusable = true
  background = Color.black

  private val menuBar = new SMenuBar(controller)

  add(menuBar, BorderPanel.Position.North)

  listenTo(keys)
  listenTo(controller)
  reactions += {
    case _: StartedGame =>
      addAndUpdatePanels()
      addAdditionalKeyReactions()
    case _: ActionExecuted =>
      addAndUpdatePanels()
    case _: Died =>
      gameOverPanels()
  }

  private def redraw(): Unit = {
    revalidate()
    repaint()
  }

  private def addAndUpdatePanels(): Unit = {
    add(new SActionPanel(controller), BorderPanel.Position.South)
    add(new SWorldGrid(controller), BorderPanel.Position.Center)
    add(new SBattleLogList(controller), BorderPanel.Position.West)
    add(new SCharacterInfo(controller), BorderPanel.Position.East)
    redraw()
  }

  private def addAdditionalKeyReactions(): Unit = {
    reactions += {
      case e: KeyPressed =>
        e.key match {
          case Key.Up => controller.move("north")
          case Key.Down => controller.move("south")
          case Key.Right => controller.move("east")
          case Key.Left => controller.move("west")
          case _ =>
        }
    }
  }

  private def gameOverPanels(): Unit = {
    _contents.clear()

    add(menuBar, BorderPanel.Position.North)
    add(new Label("YOU LOOSE") {
      foreground = Color.WHITE
    }, BorderPanel.Position.Center)
    redraw()
  }
}
