package view.gui

import controller.{Controller, StartedGame}

import scala.swing.{Dimension, Frame}

class SFrame(controller: Controller) extends Frame {
  private val contentPanel = new SPanel(controller)

  title = "RandomRPG"
  resizable = true
  preferredSize = new Dimension(1024, 640)
  contents = contentPanel
  pack()
  // Center on monitor
  peer.setLocationRelativeTo(null)

  listenTo(controller)
  reactions += {
    case _: StartedGame =>
      contentPanel.revalidate()
      contentPanel.repaint()
  }

}
