package view.gui

import java.awt.Color

import controller.Controller

import scala.swing.{BorderPanel, BoxPanel, Dimension, GridPanel, Label, Orientation}

class SCharacterInfo(controller: Controller) extends BorderPanel {
  private val currentPlayer = controller.getPlayer.get

  preferredSize = new Dimension(150, 300)
  background = Color.BLACK
  foreground = Color.WHITE

  add(new BoxPanel(Orientation.Vertical) {
    contents += getGrid(getLabel("Health:"), getLabel(currentPlayer.health.toString))
    contents += getGrid(getLabel("Attack:"), getLabel(currentPlayer.atk.toString))
    contents += getGrid(getLabel("Defense:"), getLabel(currentPlayer.defense.toString))
    contents += getGrid(getLabel("Wood:"), getLabel(currentPlayer.inventory.wood.toString))
    contents += getGrid(getLabel("Stone:"), getLabel(currentPlayer.inventory.stone.toString))
  }, BorderPanel.Position.North)


  private def getGrid(label1: Label, label2: Label): GridPanel = {
    new GridPanel(1, 1) {
      background = Color.BLACK
      contents += label1
      contents += label2
    }
  }

  private def getLabel(s: String): Label = {
    new Label(s) {
      foreground = Color.WHITE
    }
  }

}
