package view.gui

import controller.Controller

import scala.swing.event.{ButtonClicked, Key}
import scala.swing.{BoxPanel, Button, Orientation}

class SActionPanel(controller: Controller) extends BoxPanel(Orientation.Horizontal) {

  contents += new Button {
    text = "Walk North"
    reactions += {
      case ButtonClicked(_) => controller.move("north")
    }
  }
  contents += new Button {
    text = "Walk East"
    reactions += {
      case ButtonClicked(_) => controller.move("east")
    }
  }
  contents += new Button {
    text = "Walk South"
    reactions += {
      case ButtonClicked(_) => controller.move("south")
    }
  }
  contents += new Button {
    text = "Walk West"
    reactions += {
      case ButtonClicked(_) => controller.move("west")
    }
  }

  controller.getActionsForField(controller.getCurrentPosition).foreach(ac => {
    contents += new Button {
      text = ac.description
      reactions += {
        case ButtonClicked(_) => controller.executeAction(Option(ac))
      }
    }
  })

}
