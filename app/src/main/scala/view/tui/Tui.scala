package view.tui

import akka.actor.{ActorRef, ActorSystem, Props}
import controller.{ActionExecuted, Controller, Died, StartedGame}
import model.ExitActor
import model.entity.Entity
import utils.Asciis
import utils.Implicits._

import scala.swing.Reactor

class Tui(controller: Controller) extends Reactor {

  private val system = ActorSystem("ExitActor")
  private val actor: ActorRef = system.actorOf(Props[ExitActor], "ExitActor")

  listenTo(controller)
  reactions += {
    case _: StartedGame => updateConsole()
    case _: ActionExecuted => updateConsole()
    case _: Died =>
  }


  startMessage()
  printHelp()

  def executeCommand(command: String): Boolean = {
    command.toLowerCase() match {
      case "quit" => actor ! "shutdown"
      case "start" => startGame()
      case _ => executeAction(command)
    }
    true
  }

  def executeAction(command: String): Unit = {
    command.toLowerCase() match {
      case "n" => controller.move("north")
      case "e" => controller.move("east")
      case "s" => controller.move("south")
      case "w" => controller.move("west")
      case _ =>
        val action = controller.getActionsForField(controller.getCurrentPosition).find(_.identifier.equalsIgnoreCase(command))

        if (action.isDefined) {
          println("".red + controller.executeAction(action))
          println
        } else {
          println("Please enter a valid Command")
        }
    }
    printLegend()
    printWorld()
    printActions()
    availableActions()
  }

  private def printHelp(): Unit = {
    println("Help:")
    println("Start => Start a new game")
    println("Quit => Exit game")


  }

  private def startMessage(): Unit = {
    println("\n  _____                 _                   _____  _____   _____ \n |  __ \\               | |                 |  __ \\|  __ \\ / ____|\n | |__) |__ _ _ __   __| | ___  _ __ ___   | |__) | |__) | |  __ \n |  _  // _` | '_ \\ / _` |/ _ \\| '_ ` _ \\  |  _  /|  ___/| | |_ |\n | | \\ \\ (_| | | | | (_| | (_) | | | | | | | | \\ \\| |    | |__| |\n |_|  \\_\\__,_|_| |_|\\__,_|\\___/|_| |_| |_| |_|  \\_\\_|     \\_____|\n                                                                 \n                                                                 ")
  }

  private def printWorld(): Unit = {
    for (i <- 0 until controller.rowCount) {
      print("".white)
      for (j <- 0 until controller.columnCount) {
        val x: Entity = controller.getCellContent(i, j)
        x.ascii_color match {
          case "green" => print(" ".green)
          case "cyan" => print(" ".cyan)
          case "blue" => print(" ".blue)
          case "magenta" => print(" ".magenta)
          case _ => print(" ".white)
        }

        //Remove ! for testing
        if ( /*!*/ x.visited) {
          print("".white)
          print("".whiteBg)
        }
        if (x.player.isDefined) {
          print("".redBg)
        }
        //if (x.enemy != None) {
        // print("".whiteBg)
        //}

        print(x.ascii_symbol)
        print("".white)
      }
      println
    }
    println
  }

  private def availableActions(): Unit = {
    println(controller.printEnemies(controller.getCurrentPosition))
    println("Actions for field")
    controller.getActionsForField(controller.getCurrentPosition).foreach(action => println(action.identifier + " => " + action.description))

  }

  private def printActions(): Unit = {
    println("N => Walk to North")
    println("E => Walk to East")
    println("S => Walk to South")
    println("W => Walk to West")
    println()
  }

  private def printLegend(): Unit = {
    print("".green)
    print(Asciis.FOREST)
    print(" = ".white)
    print("Forest, ".white)
    print("".cyan)
    print(Asciis.LAKE)
    print(" = ".white)
    print("Lake, ".white)
    print("".magenta)
    print(Asciis.MINE)
    print(" = ".white)
    print("Mine, ".white)
    print("".blue)
    print(Asciis.SEA)
    print(" = ".white)
    print("Sea ".white)
    println
    println
  }

  private def startGame(): Unit = {
    controller.startGame()
    printLegend()
    printWorld()
    printActions()
    availableActions()
  }

  private def updateConsole(): Unit = {
    printLegend()
    printWorld()
    printActions()
    availableActions()
  }

}
