package view

import controller.Controller
import model.Game
import view.gui.SFrame
import view.tui.Tui

object Main {
  val gameWorld = Game()
  val controller = Controller(gameWorld)
  val tui = new Tui(controller)

  val sFrame = new SFrame(controller)
  sFrame.visible = true

  def main(args: Array[String]): Unit = {
    while (tui.executeCommand(scala.io.StdIn.readLine())) {}

  }


}
