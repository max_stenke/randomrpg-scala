package model

import java.util.UUID

import akka.actor.{ActorRef, ActorSystem, Props}
import model.actions.{Action, ActionType}
import model.battleLog.{BattleLog, BattleLogEntry, BattleLogStack}
import model.game.{ModelActionExecuted, ModelDied, ModelStartedGame, TGame}
import model.world.World
import model.entity._


case class Game() extends TGame {

  private val world = World(30, 30)

  private val system = ActorSystem("BattleLog")
  private val actor: ActorRef = system.actorOf(Props[BattleLog], "BattleLog")


  override def initPlayer(): Unit = {
    val world_middle_y = Math.round(world.rows / 2)
    val world_middle_x = Math.round(world.columns / 2)
    val playerPosition = Position(world_middle_x, world_middle_y)
    if (world.entityObjectAt(playerPosition).isDefined) {
      world.setEntityAt(placePlayerInEntity(world.entityObjectAt(playerPosition).get, Some(Player(UUID.randomUUID(), "Gerald", playerPosition, 100, atk = 50, defense = 20))))
    }
  }

  override def startGame(): Unit = {
    fillWorld()
    initPlayer()
    publish(ModelStartedGame())
  }

  override def rowCount(): Int = {
    world.rows
  }

  override def columnCount(): Int = {
    world.columns
  }

  override def placeEntityObject(entity: Entity): Unit = {
    world.setEntityAt(entity)
  }

  override def getCellContent(x: Int, y: Int): Entity = {
    val entityAt = world.entityObjectAt(x, y)
    entityAt.getOrElse(EmptyEntity(UUID.randomUUID(), "empty entity", Position(x, y)))
  }

  override def fillWorld(): Unit = {
    val seaWidth = Math.round(world.rows * 0.06) - 1
    val seaHeight = Math.round(world.columns * 0.06) - 1

    val res = for {
      i <- 0 until world.rows
      j <- 0 until world.columns
    } yield (i, j)
    res.foreach {
      case (i, j) =>
        if (i <= seaWidth || j <= seaHeight || i >= (world.rows - seaWidth - 1) || j >= (world.columns - seaHeight - 1)) {
          placeEntityObject(Sea(UUID.randomUUID(), "Sea", Position(i, j)))
        }
        else {
          val random = scala.util.Random
          val entityNumber = random.nextInt(3)
          val arboreal = random.nextInt(4)
          val stone = random.nextInt(3)
          val probability_enemy = random.nextInt(10)
          val random_enemy_type = random.nextInt(3)
          val enemy = if (probability_enemy < 3) None else {
            if (random_enemy_type == 0) Some(Enemy(UUID.randomUUID(), "Orc", Position(i, j), 'O', 100, atk = 20, defense = 30))
            else if (random_enemy_type == 1) Some(Enemy(UUID.randomUUID(), "Dragon", Position(i, j), 'D', 300, atk = 50, defense = 40))
            else Some(Enemy(UUID.randomUUID(), "Rabbit", Position(i, j), 'R', 20, atk = 5, defense = 5))
          }

          entityNumber match {
            case 0 =>
              arboreal match {
                case 0 => placeEntityObject(Forest(UUID.randomUUID(), "Spruce", Position(i, j), 50, enemy, player = None))
                case 1 => placeEntityObject(Forest(UUID.randomUUID(), "Oak", Position(i, j), 100, enemy, player = None))
                case 2 => placeEntityObject(Forest(UUID.randomUUID(), "Birch", Position(i, j), 70, enemy, player = None))
                case 3 => placeEntityObject(Forest(UUID.randomUUID(), "maple", Position(i, j), 85, enemy, player = None))
              }
            case 1 => placeEntityObject(Lake(UUID.randomUUID(), "Lake", Position(i, j), player = None))
            case 2 =>
              stone match {
                case 0 => placeEntityObject(Mine(UUID.randomUUID(), "Sandstone", Position(i, j), amountOfStone = 25, player = None))
                case 1 => placeEntityObject(Mine(UUID.randomUUID(), "Limestone", Position(i, j), amountOfStone = 50, player = None))
                case 2 => placeEntityObject(Mine(UUID.randomUUID(), "Granit", Position(i, j), amountOfStone = 75, player = None))
              }
          }
        }
    }
    actor ! "Game Started"
  }

  override def getActionsForField(position: Position): List[Action] = {
    val entity = world.entityObjectAt(position)
    if (entity.get.enemy.isDefined) entity.get.enemy.get.actions ::: entity.get.actions else entity.get.actions
  }

  override def executeAction(action: Option[Action]): String = {
    if (action.isDefined) {
      val currentAction = action.get
      currentAction.actionType match {
        case ActionType.Collect => collectResources()
        case ActionType.ATTACK => attackEnemy()
        case _ => "Nothing to do here"
      }
    } else {
      "Something went wrong..."
    }
  }

  override def move(direction: String): Unit = {
    actor ! "Moved " + direction
    val currentPositionEntity = world.entityObjectAt(getCurrentPosition)
    if (currentPositionEntity.isDefined) {
      val currentEntity = currentPositionEntity.get
      val player = currentEntity.player
      val futureEntity = getNextPosition(direction, currentEntity)
      if (futureEntity.isDefined) {
        if (futureEntity.get.enemy.isDefined) {
          val enemy = futureEntity.get.enemy.get
          actor ! "Encounter: " + enemy.name + ", health: " + enemy.health
        }
        futureEntity.get match {
          case _: Sea =>
          case _ =>
            world.setEntityAt(placePlayerInEntity(currentEntity, None))
            world.setEntityAt(placePlayerInEntity(futureEntity.get, player))
        }
      }
    }
    publish(ModelActionExecuted())
  }

  override def getNextPosition(direction: String, currentEntity: Entity): Option[Entity] = {
    direction match {
      case "north" => world.entityObjectAt(currentEntity.position.rowIdx - 1, currentEntity.position.columnIdx)
      case "east" => world.entityObjectAt(currentEntity.position.rowIdx, currentEntity.position.columnIdx + 1)
      case "south" => world.entityObjectAt(currentEntity.position.rowIdx + 1, currentEntity.position.columnIdx)
      case "west" => world.entityObjectAt(currentEntity.position.rowIdx, currentEntity.position.columnIdx - 1)
    }
  }

  override def getCurrentPosition: Position = {
    world.findPlayerPosition()
  }

  override def collectResources(): String = {
    val currentEntity = world.entityObjectAt(getCurrentPosition).get
    val player = currentEntity.player.get
    currentEntity match {
      case m: Mine =>
        world.setEntityAt(placePlayerInEntity(m.copy(amountOfStone = 0), Some(player.updateInventory(player.inventory.updateStone(m.amountOfStone)))))
        val response = "You collected " + m.amountOfStone.toString + " Stone"
        actor ! response
        publish(ModelActionExecuted())
        response
      case f: Forest =>
        world.setEntityAt(placePlayerInEntity(f.copy(amountOfWood = 0), Some(player.updateInventory(player.inventory.updateWood(f.amountOfWood)))))
        val response = "You collected " + f.amountOfWood.toString + " Wood"
        actor ! response
        publish(ModelActionExecuted())
        response
      case _ => "noting to get here"
    }
  }

  override def printEnemy(position: Position): String = {
    val entity = world.entityObjectAt(position)
    if (entity.get.enemy.isDefined) "A " + entity.get.enemy.get.name + " appears" else ""
  }

  override def attackEnemy(): String = {
    val currentEntityPosition = world.entityObjectAt(getCurrentPosition)
    val currentEntity = currentEntityPosition.get
    val player = currentEntity.player.get
    val enemy = currentEntity.enemy.get
    val damageDone = calculateDamage(player.atk, enemy.defense)
    val damageTaken = calculateDamage(enemy.atk, player.defense)

    actor ! "You made " + damageDone + " damage."
    actor ! "You took " + damageTaken + " damage."

    val newEnemy = enemy.takeDamage(damageDone).alive
    val newPlayer = player.takeDamage(damageTaken).alive

    if (!newEnemy.exists(e => {
      actor ! e.name + " has " + e.health + " health left."
      true
    })) actor ! enemy.name + " died."

    currentEntity match {
      case f: Forest => world.setEntityAt(placePlayerInEntity(f.updateEnemy(newEnemy), newPlayer))
    }

    publish(ModelActionExecuted())

    if (!newPlayer.get.isAlive) publish(ModelDied())

    "Attacked"
  }

  override def calculateDamage(attack: Int, defense: Int): Int = attack / 10 + defense / 10

  override def getWorld: World = {
    world
  }

  override def getBattleLog: List[BattleLogEntry] = {
    BattleLogStack.stack.toList
  }

  override def getPlayer: Option[Player] = {
    world.findPlayer()
  }

  override def placePlayerInEntity(entity: Entity, player: Option[Player]): Entity = {
    entity match {
      case m: Mine => world.setEntityAt(m.updatePlayer(player))
      case f: Forest => world.setEntityAt(f.updatePlayer(player))
      case l: Lake => world.setEntityAt(l.updatePlayer(player))
    }
  }

  override def getCellContentToText(x: Int, y: Int): List[String] = {
    val cellContent: Entity = getCellContent(x, y)
    val cellContentText: List[String] = List(cellContent.ascii_symbol.toString, cellContent.ascii_color)
    if (cellContent.player.isDefined) {
      val cellContentPlayerText = cellContentText ::: List(cellContent.player.get.ascii_symbol.toString)
      return cellContentPlayerText
    }
    cellContentText
  }

  override def getActionsToText(): List[String] = {
    val actions: List[Action] = getActionsForField(getCurrentPosition)
    val actionsText: List[String] = actions.map(a => a.description)
    actionsText
  }

  override def getPlayerInformation(): List[String] = {
    val player: Option[Player] = getPlayer
    val playerInformation: List[String] = List("Health: " + player.get.health.toString,
      "Attack: " + player.get.atk.toString,
      "Defense: " + player.get.defense.toString,
      "Wood: " + player.get.inventory.wood.toString,
      "Stone: " + player.get.inventory.stone.toString)
    playerInformation
  }

  override def getBattleLogText(): List[String] = {
    val battleLog: List[BattleLogEntry] = getBattleLog
    val battleLogText: List[String] = battleLog.map(a => a.event)
    battleLogText
  }


}
