package model.game

import model.Position
import model.actions.Action
import model.battleLog.BattleLogEntry
import model.entity.{Entity, Player}
import model.world.World

import scala.swing.Publisher
import scala.swing.event.Event

case class ModelStartedGame() extends Event

case class ModelActionExecuted() extends Event

case class ModelDied() extends Event

trait TGame extends Publisher {

  /**
    * Starts a random game
    *
    * @return a new random game
    */
  def startGame(): Unit

  /**
    *
    * @return the number of rows of the world
    */
  def rowCount(): Int

  /**
    *
    * @return the number of columns of the world
    */
  def columnCount(): Int

  /**
    * Places an entity object on the world
    */
  def placeEntityObject(entity: Entity): Unit

  /**
    *
    * @param x Row value of the world
    * @param y Column value of the world
    * @return the cell content of the given position
    */
  def getCellContent(x: Int, y: Int): Entity

  /**
    * Places random entities in the world
    */
  def fillWorld(): Unit

  /**
    *
    * @param position Position in world
    * @return list of Actions
    */
  def getActionsForField(position: Position): List[Action]

  /**
    *
    * @param action Action to execute
    * @return String with info of executed action
    */
  def executeAction(action: Option[Action]): String

  /**
    *
    * @param direction Direction the player wants to move
    */
  def move(direction: String): Unit

  /**
    *
    * @param direction     Direction the player wants to move
    * @param currentEntity current Entity where player is located
    * @return option of next Entity
    */
  def getNextPosition(direction: String, currentEntity: Entity): Option[Entity]

  /**
    *
    * @return current postion of player
    */
  def getCurrentPosition: Position

  /**
    * Collects resources on current position
    *
    * @return amount and type of collected resources
    */
  def collectResources(): String

  /**
    * Init new player
    *
    * @return new player
    */
  def initPlayer(): Unit

  /**
    * Prints the enemy name of the current player position
    *
    * @return
    */
  def printEnemy(position: Position): String

  /**
    * Calculates the fight between the player and an enemy
    */
  def attackEnemy(): String

  /**
    * Calculates the Damage between two fighting entities
    *
    * @return damage value
    */
  def calculateDamage(attacker: Int, defender: Int): Int

  /**
    *
    * @return current world
    */
  def getWorld: World

  def getBattleLog: List[BattleLogEntry]

  def getPlayer: Option[Player]

  def placePlayerInEntity(entity: Entity, player: Option[Player]): Entity

  /**
    *
    * @param x row value of the world
    * @param y column value of the world
    * @return String List of cell content
    */
  def getCellContentToText(x: Int,y: Int): List[String]

  /**
    *
    * @return String List of available Actions
    */
  def getActionsToText(): List[String]

  /**
    *
    * @return String List of player information like health and inventory
    */
  def getPlayerInformation(): List[String]

  /**
    *
    * @return String List BattleLog
    */
  def getBattleLogText(): List[String]


}
