package model

case class Position(rowIdx: Int, columnIdx: Int)