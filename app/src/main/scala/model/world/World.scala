package model.world

import model.Position
import model.entity.{Entity, Player}

case class World(rows: Int, columns: Int) extends TWorld {

  if (rows < 1 || columns < 1) {
    throw new IllegalArgumentException("no world smaller than 1 x 1")
  }

  val world: Array[Array[Entity]] = Array.ofDim[Entity](rows, columns)

  override def inBound(position: Position): Boolean = {
    (position.rowIdx >= 0 && position.columnIdx >= 0) &&
      (position.rowIdx < rows && position.columnIdx < columns)
  }

  override def setEntityAt(entity: Entity): Entity = {
    world(entity.position.rowIdx)(entity.position.columnIdx) = entity
    entity
  }

  override def entityObjectAt(position: Position): Option[Entity] = {
    entityObjectAt(position.rowIdx, position.columnIdx)
  }

  override def entityObjectAt(rowIndex: Int, colIndex: Int): Option[Entity] = {
    Option(world(rowIndex)(colIndex))
  }

  override def removeEntity(entity: Entity): Unit = {
    world(entity.position.rowIdx)(entity.position.columnIdx) = null
  }

  override def findPlayerPosition(): Position = {
    world.flatten.filter(_.player.isDefined).head.position
  }

  override def findPlayer(): Option[Player] = {
    world.flatten.filter(_.player.isDefined).head.player
  }
}

