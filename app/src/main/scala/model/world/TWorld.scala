package model.world

import model.Position
import model.entity.{Entity, Player}

trait TWorld {

  /**
    * Checks if given position is inside of the world
    *
    * @return true if position is in world
    */
  def inBound(position: Position): Boolean

  /**
    * Places an entity inside world
    *
    * @return new world
    */
  def setEntityAt(entity: Entity): Entity

  /**
    * Removes an entity from world
    *
    * @return new world
    */
  def removeEntity(entity: Entity): Unit

  /**
    * Get location of player
    *
    * @return
    */
  def findPlayerPosition(): Position

  def findPlayer(): Option[Player]

  /**
    * Get entity at position
    *
    * @param position instance of position
    * @return
    */
  def entityObjectAt(position: Position): Option[Entity]

  /**
    * Get entity at x, y position
    *
    * @param rowIndex x position
    * @param colIndex y position
    * @return
    */
  def entityObjectAt(rowIndex: Int, colIndex: Int): Option[Entity]
}




