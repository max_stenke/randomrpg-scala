package model.inventory

trait TInventory {
  val wood: Int
  val stone: Int
}
