package model.inventory

case class Inventory(override val wood: Int = 0, override val stone: Int = 0) extends TInventory {
  def updateStone(addStone: Int): Inventory = copy(stone = stone + addStone)

  def updateWood(addWood: Int): Inventory = copy(wood = wood + addWood)
}
