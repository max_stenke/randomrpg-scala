package model.actions

object ActionType extends Enumeration {
  type ActionType = Value
  val MOVE, WAIT, Collect, ATTACK, FLEE = Value
}

