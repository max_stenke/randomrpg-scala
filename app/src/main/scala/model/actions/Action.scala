package model.actions

import model.actions.ActionType.ActionType

case class Action(identifier: String, description: String, actionType: ActionType)
