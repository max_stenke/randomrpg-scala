package model.battleLog

import java.util.Calendar

import akka.actor.Actor

import scala.collection.mutable

object BattleLogStack {
  val stack = mutable.Stack[BattleLogEntry]()
}

case class BattleLogEntry(date: String, event: String)

class BattleLog extends Actor {
  override def receive: Receive = {
    case event: String =>
      BattleLogStack.stack.push(BattleLogEntry(Calendar.getInstance().getTime.toString, event))
  }
}
