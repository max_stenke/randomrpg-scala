package model.entity

import java.util.UUID

import model.Position
import model.actions.{Action, ActionType}
import model.inventory.Inventory
import utils.Asciis

abstract class Entity(val uuid: UUID, val name: String, val position: Position, val ascii_symbol: Char = Asciis.EMPTY, val ascii_color: String = "white", val visited: Boolean = false, val player: Option[Player] = None, val actions: List[Action] = List(), val enemy: Option[Enemy] = None)

case class EmptyEntity(override val uuid: UUID, override val name: String, override val position: Position) extends Entity(uuid, name, position)

case class Enemy(override val uuid: UUID, override val name: String, override val position: Position, override val ascii_symbol: Char = Asciis.ENEMY, override val health: Int, atk: Int, defense: Int) extends Entity(uuid, name, position, ascii_symbol) with THealth {
  override val actions: List[Action] = List(Action("A", "Attack Enemy", ActionType.ATTACK), Action("F", "Flee", ActionType.FLEE))

  def takeDamage(amount: Int): Enemy = copy(health = Math.max(0, health - amount))

  override def alive: Option[Enemy] = {
    if (health > 0) {
      Some(this)
    } else {
      None
    }
  }
}

case class Forest(override val uuid: UUID, override val name: String, override val position: Position, override val amountOfWood: Int, override val enemy: Option[Enemy], override val player: Option[Player], override val ascii_symbol: Char = Asciis.FOREST, override val ascii_color: String = "green") extends Entity(uuid, name, position, ascii_symbol) with TField with TForest {
  override val actions: List[Action] = List(Action("C", "Collect Resources", ActionType.Collect), Action("W", "Wait", ActionType.WAIT))

  override def updatePlayer(player: Option[Player]): Forest = copy(player = player)

  def updateEnemy(enemy: Option[Enemy]): Forest = copy(enemy = enemy)
}

case class Lake(override val uuid: UUID, override val name: String, override val position: Position, override val player: Option[Player], override val ascii_symbol: Char = Asciis.LAKE, override val ascii_color: String = "cyan") extends Entity(uuid, name, position, ascii_symbol) with TField {
  override val actions: List[Action] = List(Action("C", "Collect Resources", ActionType.Collect), Action("W", "Wait", ActionType.WAIT))

  override def updatePlayer(player: Option[Player]): Lake = copy(player = player)
}

case class Sea(override val uuid: UUID, override val name: String, override val position: Position, override val ascii_symbol: Char = Asciis.SEA, override val ascii_color: String = "blue") extends Entity(uuid, name, position, ascii_symbol)

case class Mine(override val uuid: UUID, override val name: String, override val position: Position, override val player: Option[Player], override val amountOfStone: Int, override val ascii_symbol: Char = Asciis.MINE, override val ascii_color: String = "magenta") extends Entity(uuid, name, position, ascii_symbol) with TField with TMine {
  override val actions: List[Action] = List(Action("C", "Collect Resources", ActionType.Collect), Action("W", "Wait", ActionType.WAIT))

  override def updatePlayer(player: Option[Player]): Mine = copy(player = player)
}

case class Player(override val uuid: UUID, override val name: String, override val position: Position, override val health: Int, override val ascii_symbol: Char = Asciis.PLAYER, override val ascii_color: String = "red", inventory: Inventory = Inventory(), atk: Int, defense: Int) extends Entity(uuid, name, position, ascii_symbol) with THealth {
  def updateInventory(inventory: Inventory): Player = copy(inventory = inventory)

  def takeDamage(amount: Int): Player = copy(health = Math.max(0, health - amount))

  override def alive: Option[Player] = {
    if (health > 0) {
      Some(this)
    } else {
      Some(this.copy(health = 0))
    }
  }
}
