package model.entity

trait THealth {
  val health: Int

  def alive: Option[Entity]

  def isAlive: Boolean = {
    health > 0
  }
}
