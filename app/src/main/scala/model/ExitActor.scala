package model


import akka.actor.Actor

class ExitActor extends Actor {
  override def receive: Receive = {
    case "shutdown" =>
      System.exit(1)
  }

}
