package utils

object Asciis {
  val FOREST: Char = 'Δ'
  val SEA: Char = '≈'
  val MINE: Char = 'Þ'
  val LAKE: Char = '~'
  val ENEMY: Char = '¤'
  val PLAYER: Char = '¡'
  val EMPTY: Char = 'x'
}
