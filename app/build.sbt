name := "randomRPG"

version := "0.1"

scalaVersion := "2.12.7"

libraryDependencies += "org.scala-lang.modules" % "scala-swing_2.12" % "2.0.1"
libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.5.8"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.4" % "test"