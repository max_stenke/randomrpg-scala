$(document).ready(function () {
    var gameStartedEvent = "StartedGame";
    var playerDied = "PlayerDied";

    if ("WebSocket" in window) {
        console.log("WebSocket is supported by your Browser!");
    } else {
        console.log("WebSocket NOT supported by your Browser!");
        return;
    }
    var getScriptParamUrl = function () {
        var scripts = document.getElementsByTagName('script');
        var lastScript = scripts[scripts.length - 1];
        return lastScript.getAttribute('data-url');
    };

    var url = getScriptParamUrl();
    var connection = new WebSocket(url);

    connection.onmessage = function (event) {
        var jsonData = JSON.parse(event.data);

        switch (jsonData.event) {
            case gameStartedEvent:
                createNewTable(jsonData);
                break;
            case playerDied:
                blackScreen();
                break;
            default:
                break;
        }
    }
});

function blackScreen(){
    var grid = document.getElementById("grid");
    if (grid != null) {
        grid.remove();
    }
    var grid_action = document.getElementById("grid_actions");
    if (grid_action != null) {
        grid_action.remove();
    }
    var grid_player = document.getElementById("grid_player");
    if (grid_player != null) {
        grid_player.remove();
    }
    var content = document.getElementById("content");
    var div = document.createElement('DIV');
    div.id = "div_blackScreen";
    content.appendChild(div);
    div.style.backgroundColor = "black";
    var death = createTextNode("You Loose");
    div.appendChild(death);
    div.style.color = "white";
    div.style.fontSize = '50px';
}

function createTextNode(text) {
    var text = document.createTextNode(text);
    return text;
}

function createNewTable(json) {
    var grid = document.getElementById("grid");
    if (grid != null) {
        grid.remove();
    }
    var grid_action = document.getElementById("grid_actions");
    if (grid_action != null) {
        grid_action.remove();
    }
    var grid_player = document.getElementById("grid_player");
    if (grid_player != null) {
        grid_player.remove();
    }
    var blackScreen = document.getElementById("div_blackScreen");
    if (blackScreen != null) {
        blackScreen.remove();
    }
    var battleLog = document.getElementById("grid_battleLog");
    if (battleLog != null) {
        battleLog.remove();
    }
    var content = document.getElementById("content");
    var tableGrid = document.createElement("table");
    tableGrid.id = "grid";
    content.appendChild(tableGrid);


    var tableGridBattleLog = document.createElement("table");
    tableGridBattleLog.id = "grid_battleLog";
    content.appendChild(tableGridBattleLog);
    var trHead = document.createElement('TR');
    tableGridBattleLog.appendChild(trHead);
    var th = document.createElement('TH');
    var headNode = createTextNode('Battle Log');
    th.appendChild(headNode);
    trHead.appendChild(th);
    var battleLog = json["battleLog"];
    for (var i = 0; i < battleLog.length; i++) {
        var obj = battleLog[i]["battleLog"];
        for (var j = 0; j < obj.length; j++) {
            var tr = document.createElement('TR');
            tableGridBattleLog.appendChild(tr);
            var td = document.createElement('TD');
            td.height = '50';
            td.width = '150';
            td.style.backgroundColor = "white";
            td.style.color = "black";
            tr.style.border = "1px solid black";
            var objJ = obj[j];
            if (objJ["battleLog"] !== "undefined") {
                var textNode = createTextNode(objJ);
                td.appendChild(textNode);
            }

            tr.appendChild(td);


        }
    }

    var rows = json["rows"];

    for (var i = 0; i < rows.length; i++) {
        var obj = rows[i];
        var tr = document.createElement('TR');
        tableGrid.appendChild(tr);
        console.log(obj);
        for (var j = 0; j < obj.length; j++) {
            var td = document.createElement('TD');
            td.height = '25';
            td.width = '25';
            var objJ = obj[j];
            if (objJ["ascii_symbol"] !== "undefined") {
                var textNode = createTextNode(objJ["ascii_symbol"]);
                var playerNode = createTextNode(objJ["player"]);
                td.appendChild(textNode);
                td.appendChild(playerNode);
                td.style.color = objJ["ascii_color"];
                if (objJ["player"] !== "") {
                    td.style.border = "1px solid red"
                }
            }

            tr.appendChild(td);


        }
    }

    var tableGridPlayer = document.createElement("table");
    tableGridPlayer.id = "grid_player";
    content.appendChild(tableGridPlayer);
    var trHead = document.createElement('TR');
    tableGridPlayer.appendChild(trHead);
    var th = document.createElement('TH');
    var headNode = createTextNode('Player');
    th.appendChild(headNode);
    trHead.appendChild(th);
    var player = json["player"];
    for (var i = 0; i < player.length; i++) {
        var obj = player[i]["player"];
        for (var j = 0; j < obj.length; j++) {
            var tr = document.createElement('TR');
            tableGridPlayer.appendChild(tr);
            var td = document.createElement('TD');
            td.height = '50';
            td.width = '150';
            td.style.backgroundColor = "white";
            td.style.color = "black";
            tr.style.border = "1px solid black";
            var objJ = obj[j];
            if (objJ["player"] !== "undefined") {
                var textNode = createTextNode(objJ);
                td.appendChild(textNode);
            }

            tr.appendChild(td);


        }
    }

    var tableGridActions = document.createElement("table");
    tableGridActions.id = "grid_actions";
    content.appendChild(tableGridActions);
    var trHead = document.createElement('TR');
    tableGridActions.appendChild(trHead);
    var th = document.createElement('TH');
    var headNode = createTextNode('Available actions: ');
    th.appendChild(headNode);
    trHead.appendChild(th);
    var actions = json["actions"];
    for (var i = 0; i < actions.length; i++) {
        var obj = actions[i]["action"];

        for (var j = 0; j < obj.length; j++) {
            var tr = document.createElement('TR');
            tableGridActions.appendChild(tr);
            var td = document.createElement('TD');
            td.height = '50';
            td.width = '150';
            td.style.backgroundColor = "white";
            td.style.color = "black";
            tr.style.border = "1px solid black";
            var objJ = obj[j];
            if (objJ["action"] !== "undefined") {
                var textNode = createTextNode(objJ);
                td.appendChild(textNode);
            }

            tr.appendChild(td);


        }
    }


}