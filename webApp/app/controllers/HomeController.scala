package controllers

import javax.inject.{Inject, Singleton}
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.ws.{Message, TextMessage, WebSocketRequest}
import akka.stream.Materializer
import akka.stream.scaladsl.{BroadcastHub, Flow, Keep, MergeHub, Sink, Source}
import akka.{Done, NotUsed}
import play.api.libs.json.{JsArray, JsObject, JsValue, Json}
import play.api.mvc._

import scala.Option.empty
import scala.concurrent.{ExecutionContext, Future}
import scala.swing.Reactor
import view.Main.controller
import _root_.controller.StartedGame
import _root_.controller.ActionExecuted
import _root_.controller.Died


@Singleton
class HomeController @Inject()(cc: ControllerComponents)
                              (implicit actorSystem: ActorSystem,
                               mat: Materializer,
                               executionContext: ExecutionContext,
                               webJarsUtil: org.webjars.play.WebJarsUtil,
                               appController: AppController,
                               assets: Assets)
  extends  AbstractController(cc) with Reactor   {
  private type WSMessage = String


  private var webSocketUrlOpt: Option[String] = empty
  private val assetsPreFix = "/assets/"

  listenTo(controller)

  reactions += {
    case _: StartedGame => sendStartedGameJsonToClient()
    case _: ActionExecuted => sendStartedGameJsonToClient()
    case _: Died => sendPlayerDiedJsonToClient()
  }

  private def sendPlayerDiedJsonToClient(): Unit = {
    val config = Json.obj("event" -> "PlayerDied")
    sendJsonToClient(config)
  }

  private def sendStartedGameJsonToClient(): Unit = {
    var rows = List[JsValue]()
    for (row <- 0 until appController.rowCount) {
      var columns = List[JsValue]()
      for (column <- 0 until appController.columnCount) {
        val cellContent: List[String] = appController.getCompleteCellContent(row,column)
        val player = if (cellContent.length < 3) "" else cellContent(2)
        val json = Option(Json.obj("ascii_symbol" -> cellContent(0),
                                      "ascii_color" -> cellContent(1),
                                      "player" -> player))
        columns = columns ::: List(json.get)
      }
      rows = rows ::: List(JsArray(columns))
    }
    val actionList = appController.getActions()
    val actions : Option[JsValue] = Option(Json.obj("action" -> actionList))
    val playerList = appController.getPlayerInformation()
    val player : Option[JsValue] = Option(Json.obj("player" -> playerList))
    val battleLogList = appController.getBattleLog()
    val battleLog: Option[JsValue] = Option(Json.obj("battleLog" -> battleLogList))
    val config = Json.obj(
      "event" -> "StartedGame",
      "rows" -> JsArray(rows),
      "actions" -> JsArray(List(actions.get)),
      "player" -> JsArray(List(player.get)),
      "battleLog" -> JsArray(List(battleLog.get))
    )
    sendJsonToClient(config)
  }

  private def sendJsonToClient(json: JsObject): Unit = {
    if (webSocketUrlOpt.isDefined) {
      val printSink: Sink[Message, Future[Done]] =
        Sink.foreach {
          case _: TextMessage.Strict =>
        }

      val jsonSource: Source[Message, NotUsed] =
        Source.single(TextMessage(json.toString()))

      val flow: Flow[Message, Message, Future[Done]] =
        Flow.fromSinkAndSourceMat(printSink, jsonSource)(Keep.left)
      Http().singleWebSocketRequest(WebSocketRequest(webSocketUrlOpt.get), flow)
    }
  }

  private val (socketSink, socketSource) = {
    val source = MergeHub.source[WSMessage]
      .log("source")
      .recoverWithRetries(-1, { case _: Exception ⇒ Source.empty })

    val sink = BroadcastHub.sink[WSMessage]
    source.toMat(sink)(Keep.both).run()
  }

  private val userFlow: Flow[WSMessage, WSMessage, _] = {
    Flow.fromSinkAndSource(socketSink, socketSource)
  }

  def socket(): WebSocket = {
    WebSocket.acceptOrResult[WSMessage, WSMessage] {
      _ =>
        Future.successful(userFlow).map { flow =>
          Right(flow)
        }.recover {
          case _: Exception =>
            val msg = "Cannot create websocket"
            val result = InternalServerError(msg)
            Left(result)
        }
    }
  }

  def index:Action[AnyContent] = Action { implicit request: RequestHeader =>
    webSocketUrlOpt = Option(routes.HomeController.socket().webSocketURL())
    Ok(views.html.main(webSocketUrlOpt.get, appController))
  }



}
