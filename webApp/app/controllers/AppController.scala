package controllers

import javax.inject.{Inject, Singleton}
import play.api.mvc._
import view.Main.controller

@Singleton
class AppController @Inject()(cc: ControllerComponents) extends AbstractController(cc){
  def rowCount: Int = {
    controller.rowCount
  }

  def columnCount: Int = {
    controller.columnCount
  }

  def getCompleteCellContent(rowIndex: Int, columnIndex: Int): List[String] = {
   controller.getCellContentText(rowIndex,columnIndex)
  }

  def getActions(): List[String] = {
    controller.getAvailableActionsText
  }

  def getPlayerInformation(): List[String] = {
    controller.getPlayerText
  }

  def getBattleLog(): List[String] = {
    controller.getBattleLogText
  }
}
