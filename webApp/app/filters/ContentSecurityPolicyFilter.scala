package filters

import javax.inject.{Inject, Singleton}
import controllers.routes
import scala.concurrent.{ExecutionContext, Future}
import play.api.mvc.{EssentialAction, EssentialFilter, RequestHeader}



class ContentSecurityPolicyFilter @Inject()(implicit ec: ExecutionContext) extends EssentialFilter {

  override def apply(next: EssentialAction): EssentialAction = EssentialAction { request: RequestHeader =>
    val webSocketUrl = routes.HomeController.socket().webSocketURL()(request)
    next(request).map { result =>
      result.withHeaders("Content-Security-Policy" -> s"connect-src 'self' $webSocketUrl")
    }
  }
}

