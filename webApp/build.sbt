name := "webApp"
 
version := "1.0"

lazy val core = RootProject(file("../app"))
val webapp = (project in file(".")).enablePlugins(PlayScala).dependsOn(core)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
      
resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"
      
scalaVersion := "2.12.2"

libraryDependencies ++= Seq( jdbc , ehcache , ws , specs2 % Test , guice,
  "org.webjars" %% "webjars-play" % "2.6.1",
  "org.webjars" % "bootstrap" % "3.3.4",
  "org.scala-lang.modules" % "scala-swing_2.12" % "2.0.1"
)


unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )  

      